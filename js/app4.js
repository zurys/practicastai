var ids = document.getElementById("id");

function hacerPeticion(){
    var id = ids.value;
    const url = "https://jsonplaceholder.typicode.com/users/" + id
    const http = new XMLHttpRequest;

    //Validar la respuesta

    http.onreadystatechange = function(){
        // Verifica si el estado de la solicitud es completo (readyState 4)
        if (this.readyState == 4) {
            // Verifica si la solicitud HTTP fue exitosa (código de estado 200)
            if (this.status == 200) {
                // Código ejecutado cuando la solicitud HTTP es exitosa
    
                // Obtén el elemento con el id "lista"
                let res = document.getElementById("lista");
    
                // Analiza la respuesta JSON del servidor
                const json = JSON.parse(this.responseText);
    
                // Agrega una fila de tabla con datos de la respuesta JSON
                res.innerHTML += 
                '<tr> <td class ="columna1">' + json.id + '</td>' +
                '<td class ="columna2">' + json.name + '</td>' +
                '<td class ="columna3">' + json.username + '</td>' + 
                '<td class ="columna4">' + json.email + '</td>' +
                '<td class ="columna5">' + json.address.street + ", " + json.address.suite + ", " + json.address.city + " " + 
                json.address.zipcode + " lat: " + json.address.geo.lat + " long:" + json.address.geo.long + '</td>' +
                '<td class ="columna6">' + json.phone + '</td>' +
                '<td class ="columna7">' + json.website + '</td>' +
                ' <td class ="columna8">' + json.company.name + " " + json.company.catchPhrase + " " + json.company.bs +'</td> <tr>'
    
                // Cierra la etiqueta tbody de la tabla
                res.innerHTML += "</tbody>";
    
            } else {
                // Código ejecutado cuando la solicitud HTTP no es exitosa
                alert("EL ID INGRESADO NO EXISTE");
            }
        }
    };
    

    http.open('GET', url, true);
    http.send();

}

//codificar los botones 
document.getElementById("btnCargar").addEventListener("click",function(){
    let res = document.getElementById("lista");
    res.innerHTML = "";
    hacerPeticion();
    
});