var ids = document.getElementById("id");

function hacerPeticion(){
    var id = ids.value;
    const url = "https://jsonplaceholder.typicode.com/albums/" + id
    const http = new XMLHttpRequest;

    //Validar la respuesta

    http.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                // Código ejecutado cuando la solicitud HTTP es exitosa
    
                // Obtén el elemento con el id "lista"
                let res = document.getElementById("lista");
    
                // Parsea la respuesta JSON del servidor
                const json = JSON.parse(this.responseText);
    
                // Agrega una fila de tabla con datos de la respuesta JSON
                res.innerHTML += '<tr> <td class ="columna1">' + json.id + '</td>' +
                    ' <td class ="columna2">' + json.title + '</td> </tr>'; // Cerrando correctamente la etiqueta </tr>
    
                // Cierra la etiqueta tbody de la tabla
                res.innerHTML += "</tbody>";
    
            } else {
                // Código ejecutado cuando la solicitud HTTP no es exitosa
                alert("EL ID INGRESADO NO EXISTE");
            }
        }
    };
    

    http.open('GET', url, true);
    http.send();

}

//codificar los botones 
document.getElementById("btnCargar").addEventListener("click",function(){
    let res = document.getElementById("lista");
    res.innerHTML = "";
    hacerPeticion();
    
});
